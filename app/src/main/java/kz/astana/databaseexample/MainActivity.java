package kz.astana.databaseexample;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private final String DATABASE_NAME = "contacts";
    private final int DATABASE_VERSION = 2;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyDatabaseHelper databaseHelper =
                new MyDatabaseHelper(
                        MainActivity.this,
                        DATABASE_NAME,
                        null,
                        DATABASE_VERSION
                );

        TextInputLayout nameLayout = findViewById(R.id.nameLayout);
        TextInputLayout phoneLayout = findViewById(R.id.phoneLayout);
        TextInputLayout idLayout = findViewById(R.id.idLayout);
        TextInputEditText nameEditText = findViewById(R.id.nameEditText);
        TextInputEditText phoneEditText = findViewById(R.id.phoneEditText);
        TextInputEditText idEditText = findViewById(R.id.idEditText);

        Button load = findViewById(R.id.loadButton);
        Button insert = findViewById(R.id.insertButton);
        Button update = findViewById(R.id.updateButton);
        Button delete = findViewById(R.id.deleteButton);

        ListView listView = findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                new ArrayList<>()
        );
        listView.setAdapter(adapter);


        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database = databaseHelper.getReadableDatabase();
                Cursor cursor = database.query(
                        "contacts",
                        new String[]{"id", "name", "phone"},
                        null, // "name = ? AND id = ?",
                        null, // new String[]{"Aslan", "1"},
                        null,
                        null,
                        "id ASC");
                if (cursor.moveToFirst()) {
                    int idIndex = cursor.getColumnIndex("id");
                    int nameIndex = cursor.getColumnIndex("name");
                    int phoneIndex = cursor.getColumnIndex("phone");

                    adapter.clear();
                    do {
                        String s = "ID: " + cursor.getInt(idIndex) +
                                ", name: " + cursor.getString(nameIndex) +
                                ", phone: " + cursor.getString(phoneIndex);
                        adapter.add(s);
                    } while (cursor.moveToNext());
                }
                cursor.close();
                database.close();
            }
        });

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(nameEditText.getText()) || TextUtils.isEmpty(phoneEditText.getText())) {
                    if (TextUtils.isEmpty(nameEditText.getText())) {
                        nameLayout.setError("Заполните имя");
                    }
                    if (TextUtils.isEmpty(phoneEditText.getText())) {
                        phoneLayout.setError("Заполните номер телефона");
                    }
                } else {
                    nameLayout.setError(null);
                    phoneLayout.setError(null);
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("name", nameEditText.getText().toString());
                    contentValues.put("phone", phoneEditText.getText().toString());
                    database = databaseHelper.getWritableDatabase();
                    long id = database.insert("contacts", null, contentValues);
                    Toast.makeText(MainActivity.this, "Inserted id: " + id, Toast.LENGTH_SHORT).show();
                    nameEditText.setText(null);
                    phoneEditText.setText(null);
                    database.close();
                }
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(idEditText.getText())) {
                    idLayout.setError("Заполните поле");
                } else {
                    ContentValues contentValues = new ContentValues();
                    if (!TextUtils.isEmpty(nameEditText.getText())) {
                        contentValues.put("name", nameEditText.getText().toString());
                    }
                    if (!TextUtils.isEmpty(phoneEditText.getText())) {
                        contentValues.put("phone", phoneEditText.getText().toString());
                    }
                    database = databaseHelper.getWritableDatabase();
                    int updateCount = database.update("contacts", contentValues, "id = ?", new String[]{idEditText.getText().toString()});
                    Toast.makeText(MainActivity.this, "Updated rows: " + updateCount, Toast.LENGTH_SHORT).show();
                    idEditText.setText(null);
                    database.close();
                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(idEditText.getText())) {
                    idLayout.setError("Заполните поле");
                } else {
                    database = databaseHelper.getWritableDatabase();
                    int deleteCount = database.delete("contacts", "id = ?", new String[]{idEditText.getText().toString()});
                    Toast.makeText(MainActivity.this, "Deleted rows: " + deleteCount, Toast.LENGTH_SHORT).show();
                    idEditText.setText(null);
                    database.close();
                }
            }
        });
    }
}